//declarativas
function miFuncion{
    return 3;
}
miFuncion();

function square(number) {
    return number * number;
  }
  
 // Si pasas un objeto (es decir, un valor no primitivo, como Array o un objeto definido por el usuario) como parámetro y la función cambia las propiedades del objeto, ese cambio es visible fuera de la función, como se muestra en el siguiente ejemplo:

function myFunc(theObject) {
    theObject.make = 'Toyota';
  }
  
  //[parcial]
  var mycar = { make: 'Honda', model: 'Accord', year: 1998 };
  var x, y;
  
  x = mycar.make; // x obtiene el valor "Honda"
  
  myFunc(mycar);
  y = mycar.make; // y obtiene el valor "Toyota"
                  // (la propiedad make fue cambiada por la función)  

//---------------------------------------------------------------------------------------

 //expresion o anomimas
 var miFuncion = function(a,b){
    return a + b; 
 }
 miFuncion();

//ejemplo
const square = function (number) {
    return number * number;
  };
  var x = square(4); // x obtiene el valor 16

const factorial = function fac(n) {
    return n < 2 ? 1 : n * fac(n - 1);
  };
  console.log(factorial(3));  

 //En el siguiente código, la función recibe una función definida por una expresión de función y la ejecuta por cada elemento del arreglo recibido como segundo argumento.
//mixto 
 function map(f, a) {
    let result = []; // Crea un nuevo arreglo
    let i; // Declara una variable
    for (i = 0; i != a.length; i++) result[i] = f(a[i]);
    return result;
  }
  const f = function (x) {
    return x * x * x;
  };
  let numbers = [0, 1, 2, 5, 10];
  let cube = map(f, numbers);
  console.log(cube);
 //  La función devuelve: [0, 1, 8, 125, 1000].  

 //fin funciones de expresion


function saludarEstudiantes(estudiante){
    console.log(`Hola ${estudiante}`)
};
saludarEstudiantes("Jeiber");

function sumar(a,b){
    var resultado = a + b;
    return resultado;
}
sumar(1,3);

function sumar(a,b){
    return a+b;
}
sumar(5,6);