var articulos = [
    { nombre: "Bici", costo: 3000 },
    { nombre: "TV", costo: 2500 },
    { nombre: "Libro", costo: 320 },
    { nombre: "Celular", costo: 10000 },
    { nombre: "Laptop", costo: 20000 },
    { nombre: "Teclado", costo: 500 },
    { nombre: "Audifonos", costo: 1700 },
  ];

  //Cómo utilizar el método find
  //El método find consiste en encontrar el primer elemento de un array que cumpla con la condición especificada en la función (callback). Si ningún elemento cumpla con la condición, retornará undefined.
  //array.find(function(), thisArg)

  //La función, que recibe como argumento, utiliza tres parámetros opcionales:
  //El valor actual del elemento iterado. Es decir, si es la primera iteración, será el primer elemento, y así sucesivamente.
  //El índice del elemento iterado. Es decir, si es la primera iteración, será el índice 0, y así sucesivamente.
  //El array que está iterando.
  //array.find(function(element, index, array)) 

  var algunArticulo = articulos.find(function (articulo) {
    return (articulo.nombre = "Laptop")
  })
  console.log(algunArticulo)
  /* 
  { nombre: 'Laptop', costo: 3000 }
  */
 
  //-------------------------------------
  //Cómo utilizar el método forEach
  //El método forEach de los arrays consiste en ejecutar una función (callback) para cada uno de los elementos iterados. Iterar significa repetir una acción varias veces. Este método no retorna ningún valor.
  
  //Este método recibe dos argumentos:
  
  //La función que itera cada elemento del array (obligatorio).
  //Un objeto al que puede hacer referencia el contexto this en la función. Si se lo omite, será undefined.

  articulos.forEach(function (articulo) {
    console.log(articulo)
  })
  /* 
  { nombre: 'Bici', costo: 3000 }
  { nombre: 'TV', costo: 2500 }
  ...
  { nombre: 'Audifonos', costo: 1700 }
  */

  //---------------------------
  //forEach No es necesario generar nuevo array, se utiliza para realizar un recorrido de un array principal
articulos.forEach(function(articulo){
    console.log(articulo.nombre);
});

//---------------------------
//some Se genera nuevo array, regresa un condición en Boolean
var articulosBaratos = articulos.some(function(articulo){
    return articulo.costo <= 700;
});

articulosBaratos  //regresa un true



