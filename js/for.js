var estudiantes = ["Maria", "Sergio", "Rosa", "Daniel"];

function saludarEstudiantes(alumno) {
    console.log(`Hola, ${alumno}`);
}
for(var alumno of estudiantes) {
    saludarEstudiantes(alumno);
}
Hola, Maria
Hola, Sergio
Hola, Rosa
Hola, Daniel

// otro ejemplo

var nombres = ["Andres", "Diego", "Platzi", "Ramiro", "Silvia"]

for(var i = 0; i < nombres.length; i++){
    console.log( nombres[i] )
}


//Los bucles pueden ejecutar un bloque de código varias veces. JavaScript admite diferentes tipos de bucles:

//for - recorre un bloque de código varias veces
var numbers = [5, 4, 3, 2, 1]

// ❌ No cambia el array original
for (var number of numbers) {
  number = number * 2
}

console.log(numbers) // [5, 4, 3, 2, 1]

// ✅ Cambia el array original
for(var i=0; i < numbers.length; i++){
    numbers[i] = numbers[i] * 2
}

console.log(numbers) // [ 10, 8, 6, 4, 2 ]

//for/in - recorre las propiedades de un objeto

//for/of - recorre los valores de un objeto iterable
var array = [5, 4, 3, 2, 1]

for (var elemento of array) {
  console.log(elemento) // 5 4 3 2 1
}

//while - recorre un bloque de código mientras se cumple una condición específica

//do/while - también recorre un bloque de código mientras se cumple una condición específica

var numbers = [5, 4, 3, 2, 1]
var duplicates = []

for (var number of numbers) {
  duplicates.push(number * 2)
}

console.log(duplicates) // [ 10, 8, 6, 4, 2 ]

//ejemplo con break
import java.util.Calendar;
Calendar cal = Calendar.getInstance();
cal.setTimeInMillis(System.currentTimeMillis());
int dia = cal.get(Calendar.DAY_OF_WEEK);

for (int i = 1; i <= 7; i++) {
  if (dia == i){
    System.out.println("Hoy es el " + i + "º dia de la semana.");
    break;
  }

  System.out.println("Dia " + i);
}

System.out.println("Seguimos...");

//ejemplo con continue
import java.util.Calendar;
Calendar cal = Calendar.getInstance();
cal.setTimeInMillis(System.currentTimeMillis());
int dia = cal.get(Calendar.DAY_OF_WEEK);

for (int i = 1; i <= 7; i++) {

  if (dia == i){
    continue;
  }

  System.out.println("Dia " + i);
}

System.out.println("Seguimos...");

//tablas de multiplicar
for (x=1;x>=10;x++){
  for (y=1;y<=10);y++{
    document.write(x+" *"+y+" = "+x*y)
  }
}