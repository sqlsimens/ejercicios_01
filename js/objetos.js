var miAuto = {
    marca: "Toyota",
    modelo: "Corolla",
    año: 2020, 
    detallesDelAuto: function () {
        console.log(`Auto ${this.modelo} ${this.año}`);
    }
    
    // miAuto.detallesDelAuto();
    //Auto Corolla 2020

//¿Quién es this?
//Es una variable que hace referencia al objeto.
//En este caso: this = miAuto.

//segundo ejercicio
var papa = {
    nombre: 'Jeiber',
    apellido: 'murcia garcia',
    cedula: 16883475,
    mostrar: function(){
        console.log(`Padre de ${this.nombre} ${this.apellido}`);
    }
}

//papa.mostrar();  // Padre de Jeiber murcia garcia
//papa.nombre;     // Jeiber

//objetos: Funcion constructora
function Auto(brand, model, year){
    this.marca = brand
    this.modelo = model
    this.año = year
    this.detalle = function () {
        console.log(`Auto ${this.modelo} del ${this.año}.`)
    }
}

//Si ejecutamos la función Auto mostrará un error, necesitamos especificar
// que vamos a construir una instancia mediante la palabra reservada new.

var miAuto = new Auto("Toyota", "Corolla", 2020)
/* Auto {
  marca: 'Toyota',
  modelo: 'Corolla',
  'año': 2020,
  detalle: ƒ ()
}*/

//De esta manera, puedes crear varios objetos a partir de una 
//función constructora que permita especificar atributos y métodos personalizados.

var otroAuto = new Auto("Tesla", "Model 3", 2021)
var otroAuto2 = new Auto("Suzuki", "K-20", 2019)
var otroAuto3 = new Auto("Ferrari", "Model N", 2018)


//otro ejemplo
var padre = function(nombre,apellido,hijos){
    this.nombre = nombre;
    this.apellido= apellido;
    this.hijos = hijos;
}

var padre1 = new padre('Jeiber','murcia',2);
var padre2 = new padre('estela','murcia',2);

padre1;  // padre {nombre: 'Jeiber', apellido: 'murcia', hijos: 2}
padre2;  // padre {nombre: 'estela', apellido: 'murcia', hijos: 2}



